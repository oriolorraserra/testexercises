# TestExercises



## Getting started

1. Open Anaconda Prompt
2. Install python packages, write on console:
    pip install pillow
    pip install matplotlib
    pip install numpy

3. On console, go to project directory
4. Write:
    jupyter notebook
